"""Simple Calculator"""


def add(a: int, b: int) -> int:
    """Adds two integers together"""
    return a + b


def subtract(a: int, b: int) -> int:
    """Subtracts two integers"""
    return a - b
