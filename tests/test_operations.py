"""
Test Operations from pynoxa
"""
import pytest
from pynoxa import add, subtract


@pytest.mark.parametrize(("a", "b", "c"), [
    (1, 2, 3),
    (0, 1, 1),
])
def test_add(a: int, b: int, c: int) -> None:
    """
    Test addition operations
    """
    assert c == add(a, b)


@pytest.mark.parametrize(("a", "b", "c"), [
    (3, 2, 1),
    (1, 0, 1),
])
def test_subtract(a: int, b: int, c: int) -> None:
    """
    Test addition operations
    """
    assert c == subtract(a, b)
